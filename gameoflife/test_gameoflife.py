# https://www.freecodecamp.org/news/intro-to-property-based-testing-in-python-6321e0c2f8b/
from dataclasses import dataclass, field

import pytest

alive = True
dead = False


def nextState(state, num_neighbours):
    if state == alive:
        return num_neighbours in [2, 3]
    else: 
        return num_neighbours == 3 


def test_fewer_than_2_dies():
    assert nextState(alive, 0) == dead
    assert nextState(alive, 1) == dead


def test_2_or_3_alive():
    assert nextState(alive, 2) == alive
    assert nextState(alive, 3) == alive


def test_more_than_3_dies():
    for num in range(4, 9):
        assert nextState(alive, num) == dead


def test_dead_cell_with_3_live_neighbours_reborn():
    assert nextState(dead, 3) == alive


def test_dead_cell_without_3_live_neighbours_staysdead():
    for num in range(0, 9):
        if num == 3:
            continue
        assert nextState(dead, num) == dead


def test_should_have_0_alive_neighbours_in_empty_world():
    assert GameOfLife.empty(3).count_alive_neighbours((1,1)) == 0


def test_should_have_8_alive_neighbours_in_full_world():
    assert GameOfLife.full(3).count_alive_neighbours((1,1)) == 8


def test_should_have_8_alive_in_infinite_world():
    assert GameOfLife.full(3).count_alive_neighbours((-20,75)) == 8

def test_should_handle_stillLifeBlock():
    assert True


@dataclass
class GameOfLife:
    size: int
    state: bool

    def __post_init__(self):
        self.grid = self.size * [self.size * [self.state]]

    @classmethod
    def empty(cls, size):
        return cls(size, dead)

    @classmethod
    def full(cls, size):
        return cls(size, alive)


 #   def count_alive_neighbours(self, position):
 #       r = map(lambda pos: (pos[0] + position[0], pos[1] + position[1]), self.positionsToCheck)
 #       s = filter(lambda pos: pos != position and (self.grid[pos[0]][pos[1]] == alive), r)
 #       return len(list(s))
    
    def count_alive_neighbours(self, position):
        pos_x, pos_y = (pos % self.size for pos in position) 

        alive_counter = 0
        for x in range(pos_x-1, pos_x+2):
            for y in range(pos_y-1, pos_y+2):

                alive_counter += self.grid[x][y]
        alive_counter -= self.grid[pos_x][pos_y]
        return alive_counter
    
