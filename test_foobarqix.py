## Rules  http://codingdojo.org/kata/FooBarQix/
## If the number is divisible by 3, write “Foo” instead of the number
## If the number is divisible by 5, add “Bar”
## If the number is divisible by 7, add “Qix”
## For each digit 3, 5, 7, add “Foo”, “Bar”, “Qix” in the digits order.

from functools import reduce

dict_to_string = {
    3: 'Foo',
    5: 'Bar',
    7: 'Qix'
}

def convert(number):
    result = functionalHandleDivisibilityRule(number) + functionalHandleDigitsRule(number)
    #result = handleDivisibilityRule(number) + handleDigitsRule(number)
    if result:
        return result
    else:
        return str(number)

def functionalHandleDivisibilityRule(number): 
    return ''.join([v for k, v in dict_to_string.items() if number % k == 0])


def handleDivisibilityRule(number): 
    result = ''
    for k, v in dict_to_string.items():
        if(number % k == 0):
            result += v

    return result


def handleDigitsRule(number): 
    result = ''
    for c in str(number):
        if int(c) in dict_to_string.keys():
            result += dict_to_string[int(c)]
    return result


def functionalHandleDigitsRule(number): 
    digits = list(map(lambda x: int(x), str(number)))
    digits_in_dict = list(filter(lambda x: x in dict_to_string.keys(), digits))
    return ''.join(list(map(lambda x: dict_to_string[x], digits_in_dict)))


def test_general_case():
    assert convert(1) == '1'


def test_return_foo_when_divible_by_3():
    assert convert(6) == 'Foo'


def test_return_bar_when_divible_by_5():
    assert convert(10) == 'Bar'


def test_return_qix_when_divible_by_7():
    assert convert(14) == 'Qix'


def test_return_foofoo():
    assert convert(3) == 'FooFoo'

def test_return_barbarbar():
    assert convert(55) == 'BarBarBar'


def test_return_FooQix():
    assert convert(21) == 'FooQix'